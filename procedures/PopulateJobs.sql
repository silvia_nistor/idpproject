
USE employees;
DROP PROCEDURE IF EXISTS populate_jobs;

DELIMITER //
CREATE PROCEDURE populate_jobs()
BEGIN
    CALL add_job (1, 'CEO', 15000);
    CALL add_job (2, 'Manager', 10000);
    CALL add_job (3, 'Tax Accountant', 5000);
    CALL add_job (4, 'Financial Advisor', 4500);
    CALL add_job (5, 'Sales Representative', 4000);
    CALL add_job (6, 'Legal Assistant', 3500);
    CALL add_job (7, 'Sales Specialist', 4000);
    CALL add_job (8, 'Marketing Research Analyst', 4700);
    CALL add_job (9, 'Web Developer', 6500);
    CALL add_job (10, 'Quality Engineer', 6000);
    CALL add_job (11, 'Graphic Designer', 5300);
    CALL add_job (12, 'Software Engineer', 7500);
    CALL add_job (13, 'Desktop Support Technician', 5000);
    CALL add_job (14, 'Receptionist', 3000);
    CALL add_job (15, 'HR Specialist', 4000);
    CALL add_job (16, 'Budget Analyst', 4800);
    CALL add_job (17, 'Treasurer', 6500);
    CALL add_job (18, 'Business Analyst', 4500);
    CALL add_job (19, 'Help Desk Specialist', 3500);
    CALL add_job (20, 'Data Analyst', 5100);
    CALL add_job (21, 'Database Administrator', 6300);
    CALL add_job (22, 'Technical Writer', 4600);
    CALL add_job (23, 'Front Desk Agent', 3200);
    CALL add_job (24, 'Contract Specialist', 4400);
    CALL add_job (25, 'Batch Operator', 3300);
END //
DELIMITER ;
