USE employees;
DROP PROCEDURE IF EXISTS add_project;
DROP PROCEDURE IF EXISTS add_department;
DROP PROCEDURE IF EXISTS add_job;
DROP PROCEDURE IF EXISTS add_employee;
DROP PROCEDURE IF EXISTS add_employee_project;

-- insert into tables
DELIMITER //
CREATE PROCEDURE add_project(
	IN id INT, IN name VARCHAR(40), IN startDate VARCHAR(40), IN endDate VARCHAR(40)
)
BEGIN
	INSERT INTO Projects VALUES (id, name, STR_TO_DATE(startDate, '%d-%m-%Y'), STR_TO_DATE(endDate, '%d-%m-%Y'));
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE add_department(
	IN id INT, IN name VARCHAR(40), IN city VARCHAR(40), IN country VARCHAR(40)
)
BEGIN
	INSERT INTO Departments VALUES (id, name, city, country);
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE add_job(
	IN id INT, IN jobTitle VARCHAR(40), IN sal INT
)
BEGIN
	INSERT INTO Jobs VALUES (id, jobTitle, sal);
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE add_employee(
	IN empId INT, IN depId INT, IN mgrId INT, IN jobId INT, IN firstName VARCHAR(40),
	IN lastName VARCHAR(40), IN birthDate VARCHAR(40), IN bonus INT, IN hiredate VARCHAR(40)
)
BEGIN
		INSERT INTO Employees VALUES (empId, depId, mgrId, jobId, firstName, lastName, STR_TO_DATE(birthDate, '%d-%m-%Y'), bonus, STR_TO_DATE(hiredate, '%d-%m-%Y'));
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE add_employee_project(
	IN projId INT, IN empId INT
)
BEGIN
	DECLARE value INT;

	CALL check_id_exists(projId, empId, 'EmployeeProject', value);

	IF value = 0 THEN
		INSERT INTO EmployeeProject VALUES (projId, empId);
    ELSE
        SIGNAL SQLSTATE '45002' SET MESSAGE_TEXT = 'Pair already exists!';
	END IF;
END //
DELIMITER ;
