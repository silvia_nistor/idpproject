import mysql.connector
from mysql.connector import errorcode
from pymysql import MySQLError

HOST ='procedures'
DATABASE ='employees'
USER ='root'
PASSWORD ='password'

def show_projects():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_projects', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_departments():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_departments', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")


    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_jobs():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_jobs', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_employees():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_employees', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret


def select_project(projectId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_project', args=(projectId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_department(departmentId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_department', args=(departmentId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")


    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_job(jobId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_job', args=(jobId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_employee(employeeId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_employee', args=(employeeId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_employee_detailed(employeeId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_employee_detailed', args=(employeeId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret


def employees_info():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('employees_info', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def highest_nr_projects():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('highest_nr_projects', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def emp_max_sal_in_dep(dep_id):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('emp_max_sal_in_dep', args=(dep_id,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret


def get_employees_with_seniority(seniority):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('get_employees_with_seniority', args=(seniority,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret
