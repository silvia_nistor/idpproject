import utils.admin as dbmanager
from flask import Flask, render_template, request
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import Counter, Histogram, Gauge, Summary

app = Flask(__name__)
metrics = PrometheusMetrics(app)

metrics.info('app_info', 'Application info', version='1.0.3')

@app.route('/')
def idp_project():
   return render_template('index.html')

@app.route("/show-projects", methods=["GET"])
def show_projects():
    try:
        data = dbmanager.show_projects()
        return render_template(
            "table.html",
            tableName='Projects',
            data=data,
            header=['No', 'Project Name', 'Start Date', 'End Date']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-departments", methods=["GET"])
def show_departments():
    try:
        data = dbmanager.show_departments()
        return render_template(
            "table.html",
            tableName='Departments',
            data=data,
            header=['No', 'Name', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-jobs", methods=["GET"])
def show_jobs():
    try:
        data = dbmanager.show_jobs()
        return render_template(
            "table.html",
            tableName='Jobs',
            data=data,
            header=['No', 'Title', 'Salary']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-employees", methods=["GET"])
def show_employees():
    try:
        data = dbmanager.show_employees()
        return render_template(
            "table.html",
            tableName='Employees',
            data=data,
            header=['No', 'Dept. ID', 'Mgr. ID', 'Job ID',
                    'First Name', 'Last Name', 'Birth Date',
                    'Bonus', 'Hire Date']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-employees-info", methods=["GET"])
def show_employees_info():
    try:
        data = dbmanager.employees_info()
        return render_template(
            "table.html",
            tableName='Employees Info',
            data=data,
            header=['No', 'Name', 'Birth Date', 'Hire Date',
                    'Salary', 'Bonus', 'Current Job',
                    'Current Projects', 'Manager Name',
                    'Department', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-employee-project", methods=["GET"])
def show_employee_project():
    try:
        data = dbmanager.show_employee_project()
        return render_template(
            "table.html",
            tableName='Employee Projects',
            data=data,
            header=['Project ID', 'Employee ID']
        )
    except Exception as e:
        return (str(e))

@app.route("/most-active-employees", methods=["GET"])
def highest_nr_projects():
    try:
        data = dbmanager.highest_nr_projects()
        return render_template(
            "table.html",
            tableName='Most Active Employees',
            data=data,
            header=['No', 'Name', 'Birth Date', 'Hire Date',
                    'Salary', 'Bonus', 'Current Job',
                    'No. of Projects', 'Manager Name',
                    'Department', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/most-rewarded-employees", methods=["POST"])
def emp_max_sal_in_dep():
    try:
        data = dbmanager.emp_max_sal_in_dep(request.form["deptID"])
        return render_template(
            "table.html",
            tableName='Most Rewarded Employees',
            data=data,
            header=['No', 'Name', 'Salary']
        )
    except Exception as e:
        return (str(e))

@app.route("/show-senior-employees", methods=["POST"])
def get_employees_with_seniority():
    try:
        data = dbmanager.get_employees_with_seniority(request.form["seniority"])
        return render_template(
            "table.html",
            tableName='Senior Employees',
            data=data,
            header=['No', 'Name', 'Birth Date', 'Hire Date',
                    'Salary', 'Bonus', 'Current Job',
                    'Current Projects', 'Manager Name',
                    'Department', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/select-project", methods=["POST"])
def select_project():
    try:
        data = dbmanager.select_project(request.form["projID"])
        return render_template(
            "table.html",
            tableName='Project',
            data=data,
            header=['ID', 'Name', 'StartDate', 'EndDate']
        )
    except Exception as e:
        return (str(e))

@app.route("/select-department", methods=["POST"])
def select_department():
    try:
        data = dbmanager.select_department(request.form["deptID"])
        return render_template(
            "table.html",
            tableName='Department',
            data=data,
            header=['ID', 'Name', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/select-job", methods=["POST"])
def select_job():
    try:
        data = dbmanager.select_job(request.form["jobID"])
        return render_template(
            "table.html",
            tableName='Job',
            data=data,
            header=['ID', 'Job Title', 'Salary']
        )
    except Exception as e:
        return (str(e))

@app.route("/select-employee", methods=["POST"])
def select_employee():
    try:
        data = dbmanager.select_employee(request.form["empID"])
        return render_template(
            "table.html",
            tableName='Project',
            data=data,
            header=['No', 'Dept. ID', 'Mgr. ID', 'Job ID',
                    'First Name', 'Last Name', 'Birth Date',
                    'Bonus', 'Hire Date']
        )
    except Exception as e:
        return (str(e))

@app.route("/select-employee-detailed", methods=["POST"])
def select_employee_detailed():
    try:
        data = dbmanager.select_employee_detailed(request.form["empID"])
        return render_template(
            "table.html",
            tableName='Project',
            data=data,
            header=['No', 'Name', 'Birth Date', 'Hire Date',
                    'Salary', 'Bonus', 'Current Job',
                    'Current Projects', 'Manager Name',
                    'Department', 'City', 'Country']
        )
    except Exception as e:
        return (str(e))

@app.route("/delete-project", methods=["POST"])
def delete_project():
    try:
        ret = dbmanager.delete_project(request.form["projID"])
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/delete-department", methods=["POST"])
def delete_department():
    try:
        ret = dbmanager.delete_department(request.form["deptID"])
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/delete-job", methods=["POST"])
def delete_job():
    try:
        ret = dbmanager.delete_job(request.form["jobID"])
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/delete-employee", methods=["POST"])
def delete_employee():
    try:
        ret = dbmanager.delete_employee(request.form["empID"])
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/delete-employee-project", methods=["GET"])
def delete_employee_project_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Delete employee-project pair",
            formAction="/delete-employee-project",
            fields=[
                ('Project ID', 'project id'), ('Employee ID', 'employee id')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/delete-employee-project", methods=["POST"])
def delete_employee_project_post():
    try:
        ret = dbmanager.delete_employee_project(
            request.form["project id"],
            request.form["employee id"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/add-project", methods=["GET"])
def add_project_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Add project",
            formAction="/add-project",
            fields=[
                ('ID', 'id'), ('Name', 'name'), ('Start Date', 'startDate'), ('End Date', 'endDate')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/add-project", methods=["POST"])
def add_project_post():
    try:
        ret = dbmanager.add_project(
            request.form["id"],
            request.form["name"],
            request.form["startDate"],
            request.form["endDate"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/add-department", methods=["GET"])
def add_department_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Add department",
            formAction="/add-department",
            fields=[
                ('ID', 'id'), ('Name', 'name'), ('City', 'city'), ('Country', 'country')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/add-department", methods=["POST"])
def add_department_post():
    try:
        ret = dbmanager.add_department(
            request.form["id"],
            request.form["name"],
            request.form["city"],
            request.form["country"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/add-job", methods=["GET"])
def add_job_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Add job",
            formAction="/add-job",
            fields=[
                ('ID', 'id'), ('Title', 'title'), ('Salary', 'salary')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/add-job", methods=["POST"])
def add_job_post():
    try:
        ret = dbmanager.add_job(
            request.form["id"],
            request.form["title"],
            request.form["salary"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/add-employee", methods=["GET"])
def add_employee_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Add employee",
            formAction="/add-employee",
            fields=[
                ('No', 'no'), ('Dept. ID', 'dept. id'), ('Mgr. ID', 'mgr. id'),
                ('Job ID', 'job id'), ('First Name', 'first name'),
                ('Last Name', 'last name'), ('Birth Date', 'birth date'),
                ('Bonus', 'bonus'), ('Hire Date', 'hire date')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/add-employee", methods=["POST"])
def add_employee_post():
    try:
        ret = dbmanager.add_employee(
            request.form["no"],
            request.form["dept. id"],
            request.form["mgr. id"],
            request.form["job id"],
            request.form["first name"],
            request.form["last name"],
            request.form["birth date"],
            request.form["bonus"],
            request.form["hire date"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/add-employee-project", methods=["GET"])
def add_employee_project_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Add employee-project pair",
            formAction="/add-employee-project",
            fields=[
                ('Project ID', 'project id'), ('Employee ID', 'employee id')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/add-employee-project", methods=["POST"])
def add_employee_project_post():
    try:
        ret = dbmanager.add_employee_project(
            request.form["project id"],
            request.form["employee id"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/update-project", methods=["GET"])
def update_project_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Update project",
            formAction="/update-project",
            fields=[
                ('ID', 'id'), ('Name', 'name'), ('Start Date', 'startDate'), ('End Date', 'endDate')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/update-project", methods=["POST"])
def update_project_post():
    try:
        ret = dbmanager.update_project(
            request.form["id"],
            request.form["name"],
            request.form["startDate"],
            request.form["endDate"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/update-department", methods=["GET"])
def update_department_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Update department",
            formAction="/update-department",
            fields=[
                ('ID', 'id'), ('Name', 'name'), ('City', 'city'), ('Country', 'country')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/update-department", methods=["POST"])
def update_department_post():
    try:
        ret = dbmanager.update_department(
            request.form["id"],
            request.form["name"],
            request.form["city"],
            request.form["country"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/update-job", methods=["GET"])
def update_job_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Update job",
            formAction="/update-job",
            fields=[
                ('ID', 'id'), ('Title', 'title'), ('Salary', 'salary')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/update-job", methods=["POST"])
def update_job_post():
    try:
        ret = dbmanager.update_job(
            request.form["id"],
            request.form["title"],
            request.form["salary"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/update-employee", methods=["GET"])
def update_employee_get():
    try:
        return render_template(
            "form.html",
            pageTitle="Update employee",
            formAction="/update-employee",
            fields=[
                ('No', 'no'), ('Dept. ID', 'dept. id'), ('Mgr. ID', 'mgr. id'),
                ('Job ID', 'job id'), ('First Name', 'first name'),
                ('Last Name', 'last name'), ('Birth Date', 'birth date'),
                ('Bonus', 'bonus'), ('Hire Date', 'hire date')
            ]
        )
    except Exception as e:
        return (str(e))

@app.route("/update-employee", methods=["POST"])
def update_employee_post():
    try:
        ret = dbmanager.update_employee(
            request.form["no"],
            request.form["dept. id"],
            request.form["mgr. id"],
            request.form["job id"],
            request.form["first name"],
            request.form["last name"],
            request.form["birth date"],
            request.form["bonus"],
            request.form["hire date"]
        )
        if ret[0] == -1:
            return render_template("message.html", value=str(ret[1]))
        return render_template('index.html')
    except Exception as e:
        return (str(e))

@app.route("/delete-tables", methods=["GET"])
def delete_tables_get():
    try:
        ret = dbmanager.drop_all_tables()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/create-tables", methods=["GET"])
def create_tables_get():
    try:
        ret = dbmanager.create_all_tables()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/populate-projects", methods=["GET"])
def populate_projects_get():
    try:
        ret = dbmanager.populate_projects()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/populate-departments", methods=["GET"])
def populate_departments_get():
    try:
        ret = dbmanager.populate_departments()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/populate-jobs", methods=["GET"])
def populate_jobs_get():
    try:
        ret = dbmanager.populate_jobs()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/populate-employees", methods=["GET"])
def populate_employees_get():
    try:
        ret = dbmanager.populate_employees()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))

@app.route("/populate-employee-project", methods=["GET"])
def populate_employee_project_get():
    try:
        ret = dbmanager.populate_employee_project()
        return render_template("message.html", value=str(ret[1]))
    except Exception as e:
        return (str(e))


@app.route("/update-bonus", methods=["GET"])
def update_bonus():
    try:
        dbmanager.update_bonus()
        message = "Bonus updated for all employees!"
        return render_template("message.html", value=message)
    except Exception as e:
        return (str(e))


if __name__ == '__main__':
    app.run('0.0.0.0', debug=False)
