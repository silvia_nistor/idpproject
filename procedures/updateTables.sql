
USE employees;
DROP PROCEDURE IF EXISTS update_project;
DROP PROCEDURE IF EXISTS update_department;
DROP PROCEDURE IF EXISTS update_job;
DROP PROCEDURE IF EXISTS update_employee;


-- update tables
DELIMITER //
CREATE PROCEDURE update_project(
	IN id INT, IN name VARCHAR(40), IN startDate VARCHAR(40), IN endDate VARCHAR(40)
)
BEGIN
	UPDATE Projects
	SET Name = name, StartDate = STR_TO_DATE(startDate, '%d-%m-%Y'), EndDate = STR_TO_DATE(endDate, '%d-%m-%Y')
	WHERE Projects.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE update_department(
	IN id INT, IN name VARCHAR(40), IN city VARCHAR(40), IN country VARCHAR(40)
)
BEGIN
	UPDATE Departments
	SET Name = name, City = city, Country = country
	WHERE Departments.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE update_job(
	IN id INT, IN jobTitle VARCHAR(40), IN sal INT
)
BEGIN
	UPDATE Jobs
    SET JobTitle = jobTitle, Sal = sal
    WHERE Jobs.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE update_employee(
	IN empId INT, IN depId INT, IN mgrId INT, IN jobId INT, IN firstName VARCHAR(40),
	IN lastName VARCHAR(40), IN birthDate VARCHAR(40), IN bonus INT, IN hiredate VARCHAR(40)
)
BEGIN
    UPDATE Employees
    SET
        DepID = depId,
        MgrID = mgrId,
        JobID = jobId,
        FirstName = firstName,
        LastName = lastName,
        BirthDate = STR_TO_DATE(birthDate, '%d-%m-%Y'),
        Bonus = bonus,
        Hiredate = STR_TO_DATE(hiredate, '%d-%m-%Y')
    WHERE Employees.EmpID = empId;
END //
DELIMITER ;
