USE employees;
DROP PROCEDURE IF EXISTS delete_project;
DROP PROCEDURE IF EXISTS delete_department;
DROP PROCEDURE IF EXISTS delete_job;
DROP PROCEDURE IF EXISTS delete_employee;
DROP PROCEDURE IF EXISTS delete_employee_project;


-- delete from tables
DELIMITER //
CREATE PROCEDURE delete_project(
	IN id INT
)
BEGIN
	DELETE FROM Projects
	WHERE Projects.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE delete_department(
	IN id INT
)
BEGIN
	DELETE FROM Departments
	WHERE Departments.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE delete_job(
	IN id INT
)
BEGIN
	DELETE FROM Jobs
    WHERE Jobs.ID = id;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE delete_employee(
	IN empId INT
)
BEGIN
    DELETE FROM Employees
    WHERE Employees.EmpID = empId;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE delete_employee_project(
	IN projId INT, IN empId INT
)
BEGIN
    DELETE FROM EmployeeProject
    WHERE EmployeeProject.ProjectID = projId AND EmployeeProject.EmpID = empId;
END //
DELIMITER ;
