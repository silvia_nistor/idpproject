import utils.admin as dbmanager
from flask import Flask, render_template, request
from flask import jsonify, request, Flask
from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import Counter, Histogram, Gauge, Summary

app = Flask(__name__)
metrics = PrometheusMetrics(app)

metrics.info('app_info', 'Application info', version='1.0.3')

@app.route('/')
def idp_project():
   return "It works!"


@app.route("/show-projects", methods=["GET"])
def show_projects():
    try:
        data = dbmanager.show_projects()
        res = {'result' : data}
        return jsonify(res), 100
    except Exception as e:
        return (str(e))

@app.route("/show-departments", methods=["GET"])
def show_departments():
    try:
        data = dbmanager.show_departments()
        res = {'result' : data}
        return jsonify(res), 200
    except Exception as e:
        return (str(e))

@app.route("/show-jobs", methods=["GET"])
def show_jobs():
    try:
        data = dbmanager.show_jobs()
        res = {'result' : data}
        return jsonify(res), 300
    except Exception as e:
        return (str(e))

@app.route("/show-employees", methods=["GET"])
def show_employees():
    try:
        data = dbmanager.show_employees()
        res = {'result' : data}
        return jsonify(res), 400
    except Exception as e:
        return (str(e))

@app.route("/show-employees-info", methods=["GET"])
def show_employees_info():
    try:
        data = dbmanager.employees_info()
        res = {'result' : data}
        return jsonify(res), 500
    except Exception as e:
        return (str(e))

@app.route("/show-employee-project", methods=["GET"])
def show_employee_project():
    try:
        data = dbmanager.show_employee_project()
        res = {'result' : data}
        return jsonify(res), 600
    except Exception as e:
        return (str(e))

@app.route("/most-active-employees", methods=["GET"])
def highest_nr_projects():
    try:
        data = dbmanager.highest_nr_projects()
        res = {'result' : data}
        return jsonify(res), 700
    except Exception as e:
        return (str(e))

@app.route("/most-rewarded-employees", methods=["POST"])
def emp_max_sal_in_dep():
    id = request.args.get('id')
    try:
        data = dbmanager.emp_max_sal_in_dep(id)
        res = {'result' : data}
        return jsonify(res), 800
    except Exception as e:
        return (str(e))

@app.route("/show-senior-employees", methods=["POST"])
def get_employees_with_seniority():
    seniority = request.args.get('id')
    try:
        data = dbmanager.get_employees_with_seniority(seniority)
        res = {'result' : data}
        return jsonify(res), 900
    except Exception as e:
        return (str(e))

@app.route("/select-project", methods=["POST"])
def select_project():
    id = request.args.get('id')
    try:
        data = dbmanager.select_project(id)
        res = {'result' : data}
        return jsonify(res), 1000
    except Exception as e:
        return (str(e))

@app.route("/select-department", methods=["POST"])
def select_department():
    id = request.args.get('id')
    try:
        data = dbmanager.select_department(id)
        res = {'result' : data}
        return jsonify(res), 1100
    except Exception as e:
        return (str(e))

@app.route("/select-job", methods=["POST"])
def select_job():
    id = request.args.get('id')
    try:
        data = dbmanager.select_job(id)
        res = {'result' : data}
        return jsonify(res), 1200
    except Exception as e:
        return (str(e))

@app.route("/select-employee", methods=["POST"])
def select_employee():
    id = request.args.get('id')
    try:
        data = dbmanager.select_employee(id)
        res = {'result' : data}
        return jsonify(res), 1300
    except Exception as e:
        return (str(e))

@app.route("/select-employee-detailed", methods=["POST"])
def select_employee_detailed():
    id = request.args.get('id')
    try:
        data = dbmanager.select_employee_detailed(id)
        res = {'result' : data}
        return jsonify(res), 1400
    except Exception as e:
        return (str(e))


if __name__ == '__main__':
    app.run('0.0.0.0', port=5555, debug=False)
