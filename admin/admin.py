import mysql.connector
from mysql.connector import errorcode
from pymysql import MySQLError

HOST = 'procedures'
DATABASE ='employees'
USER ='root'
PASSWORD ='password'


def drop_all_tables():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('drop_tables', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "All tables deleted!")

def create_all_tables():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('create_tables', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Tables created!")

def populate_projects():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('populate_projects', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Projects table has been populated!")

def populate_departments():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('populate_departments', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Departments table has been populated!")


def populate_jobs():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('populate_jobs', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Jobs table has been populated!")



def populate_employees():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('populate_employees', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employees table has been populated!")



def populate_employee_project():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('populate_employee_project', args = ())
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee-Project table has been populated!")


def check_id_exists(id1, id2, tableName, result):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            result_args = cursor.callproc('check_id_exists', args = (id1, id2, tableName, result))
            return result_args[3]
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()

def show_projects():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_projects', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_departments():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_departments', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")


    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_jobs():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_jobs', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_employees():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_employees', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def show_employee_project():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('show_employee_project', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret


def select_project(projectId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_project', args=(projectId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_department(departmentId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_department', args=(departmentId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")


    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_job(jobId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_job', args=(jobId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_employee(employeeId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_employee', args=(employeeId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def select_employee_detailed(employeeId):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('select_employee_detailed', args=(employeeId,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def add_project(id, name, startDate, endDate):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('add_project', args=(id, name, startDate, endDate))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Project added!")

def add_department(id, name, city, country):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('add_department', args=(id, name, city, country))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Department added!")

def add_job(id, jobTitle, sal):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('add_job', args=(id, jobTitle, sal))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Job added!")

def add_employee(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('add_employee', args=(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee added!")

def add_employee_project(projId, empId):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('add_employee_project', args=(projId, empId))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee-Project pair added!")

def update_project(id1, name, startDate, endDate):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('update_project', args=(id1, name, startDate, endDate))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Project updated!")


def update_department(id, name, city, country):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('update_department', args=(id, name, city, country))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Department updated!")


def update_job(id, jobTitle, sal):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('update_job', args=(id, jobTitle, sal))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Job updated!")


def update_employee(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('update_employee', args=(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee updated!")


def delete_project(id):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('delete_project', args=(id,))
            connection.commit()

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Project deleted!")


def delete_department(id):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('delete_department', args=(id,))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Department deleted!")


def delete_job(id):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('delete_job', args=(id,))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Job deleted!")


def delete_employee(id):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('delete_employee', args=(id,))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee deleted!")


def delete_employee_project(projId, empId):
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('delete_employee_project', args=(projId, empId))
            connection.commit()
    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            return (-1, "Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            return (-1, "Database does not exist")
        else:
            return (-1, err)
    else:
        connection.close()
        return (1, "Employee-Project pair deleted!")


def employees_info():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('employees_info', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def highest_nr_projects():
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('highest_nr_projects', args=())
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

def update_bonus():
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('update_bonus', args=())
            connection.commit()

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()

def emp_max_sal_in_dep(dep_id):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('emp_max_sal_in_dep', args=(dep_id,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret


def get_employees_with_seniority(seniority):
    ret = []
    try:
        connection = mysql.connector.connect(host=HOST,
                                            database=DATABASE,
                                            user=USER,
                                            password=PASSWORD)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.callproc('get_employees_with_seniority', args=(seniority,))
            for list in cursor.stored_results():
                ret = list.fetchall()
            for row in ret:
                print(row, end = "\n")

    except MySQLError as e:
        return (-1, e)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        connection.close()
        return ret

if __name__ == '__main__':
    while True:
        print(
            "\nWelcome to admin menu! Here are your options (Choose one):\n"
            "1. Drop Tables\n2. Create Tables\n3. Populate tables\n"
            "4. Add Project\n5. Add Department\n6. Add Job\n"
            "7. Add Employee\n8. Add Pair Project-Employee\n9. Show Projects\n"
            "10. Show Departments\n11. Show Jobs\n12. Show Employees\n"
            "13. Show Project Employees\n14. Select Project\n"
            "15. Select Department\n16. Select Job\n17. Select Employee\n"
            "18. Select Employee Detailed\n19. Update Project\n20. Update Department\n"
            "21. Update Job\n22. Update Employee\n23. Delete Project\n24. Delete Department\n"
            "25. Delete Job\n26. Delete Employee\n27. Delete Pair Project-Employee\n"
            "28. Show Employees Info\n29. Show Employees With Highest Number Of Projects\n"
            "30. Show Employees With Maximum Salary In A Given Department\n"
            "31. Show Employees with certain seniority\n"
            "32. Update Bonus\n33. Exit\n\n")
        line = input()
        if line == '1':
            drop_all_tables()
        elif line == "2":
            create_all_tables()
        elif line == "3":
            populate_projects()
            populate_departments()
            populate_jobs()
            populate_employees()
            populate_employee_project()
        elif line == "4":
            print("\nInsert parameters for new project:\n")
            print("ID = ")
            id = input()
            print("\nName = ")
            name = input()
            print("\nStart Date = ")
            startDate = input()
            print("\nEnd Date = ")
            endDate = input()
            add_project(id, name, startDate, endDate)
        elif line == "5":
            print("\nInsert parameters for new department:\n")
            print("ID = ")
            id = input()
            print("\nName = ")
            name = input()
            print("\nCity = ")
            city = input()
            print("\nCountry = ")
            country = input()
            add_department(id, name, city, country)
        elif line == "6":
            print("\nInsert parameters for new job:\n")
            print("ID = ")
            id = input()
            print("\nJob Title = ")
            jobTitle = input()
            print("\nSalary = ")
            sal = input()
            add_job(id, jobTitle, sal)
        elif line == "7":
            print("\nInsert parameters for new employee:\n")
            print("Employee ID = ")
            empId = input()
            print("Department ID = ")
            depId = input()
            print("Manager ID = ")
            mgrId = input()
            print("Job ID = ")
            jobId = input()
            print("\nFirst Name = ")
            firstName = input()
            print("\nLast Name = ")
            lastName = input()
            print("\nBirth Date = ")
            birthDate = input()
            print("\nBonus = ")
            bonus = input()
            print("\nHire Date = ")
            hireDate = input()
            add_employee(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate)
        elif line == "8":
            print("\nInsert parameters for new pair project-employee:\n")
            print("Project ID = ")
            projId = input()
            print("\nEmployee ID = ")
            empId = input()
            add_employee_project(projId, empId)
        elif line == "9":
            show_projects()
        elif line == "10":
            show_departments()
        elif line == "11":
            show_jobs()
        elif line == "12":
            show_employees()
        elif line == "13":
            show_employee_project()
        elif line == "14":
            print("\nInsert parameters for project:\n")
            projId = input()
            select_project(projId)
        elif line == "15":
            print("\nInsert parameters for department:\n")
            depId = input()
            select_department(depId)
        elif line == "16":
            print("\nInsert parameters for job:\n")
            jobId = input()
            select_job(jobId)
        elif line == "17":
            print("\nInsert parameters for employee:\n")
            empId = input()
            select_employee(empId)
        elif line == "18":
            print("\nInsert parameters for employee:\n")
            empId = input()
            select_employee_detailed(empId)
        elif line == "19":
            print("\nInsert parameters for updating project:\n")
            print("ID = ")
            id = input()
            print("\nName = ")
            name = input()
            print("\nStart Date = ")
            startDate = input()
            print("\nEnd Date = ")
            endDate = input()
            update_project(id, name, startDate, endDate)
        elif line == "20":
            print("\nInsert parameters for updating department:\n")
            print("ID = ")
            id = input()
            print("\nName = ")
            name = input()
            print("\nCity = ")
            city = input()
            print("\nCountry = ")
            country = input()
            update_department(id, name, city, country)
        elif line == "21":
            print("\nInsert parameters for updating job:\n")
            print("ID = ")
            id = input()
            print("\nJob Title = ")
            jobTitle = input()
            print("\nSalary = ")
            sal = input()
            update_job(id, jobTitle, sal)
        elif line == "22":
            print("\nInsert parameters for updating employee:\n")
            print("Employee ID = ")
            empId = input()
            print("Department ID = ")
            depId = input()
            print("Manager ID = ")
            mgrId = input()
            print("Job ID = ")
            jobId = input()
            print("\nFirst Name = ")
            firstName = input()
            print("\nLast Name = ")
            lastName = input()
            print("\nBirth Date = ")
            birthDate = input()
            print("\nBonus = ")
            bonus = input()
            print("\nHire Date = ")
            hireDate = input()
            update_employee(empId, depId, mgrId, jobId, firstName, lastName, birthDate, bonus, hireDate)
        elif line == "23":
            print("\nInsert ID for deleting project:\n")
            projId = input()
            delete_project(projId)
        elif line == "24":
            print("\nInsert ID for deleting department:\n")
            depId = input()
            delete_department(depId)
        elif line == "25":
            print("\nInsert ID for deleting job:\n")
            jobId = input()
            delete_job(jobId)
        elif line == "26":
            print("\nInsert ID for deleting employee:\n")
            empId = input()
            delete_employee(empId)
        elif line == "27":
            print("\nInsert parameters for deleting project-employee pair:\n")
            print("\nProject ID = ")
            projId = input()
            print("\nEmployee ID = ")
            empId = input()
            delete_employee_project(projId, empId)
        elif line == "28":
            employees_info()
        elif line == "29":
            highest_nr_projects()
        elif line == "30":
            print("\nInsert department ID:\n")
            depId = input()
            emp_max_sal_in_dep(depId)
        elif line == "31":
            print("\nInsert seniority:\n")
            seniority = input()
            get_employees_with_seniority(seniority)
        elif line == "32":
            update_bonus()
        elif line == "33":
            break
        else:
            print("Invalid option! Choose a correct option!\n")

    # update_employee(2, 216, 23, 30, 'Na', 'LA', '23-06-2018', 31, '06-03-2010')
    # result = 0
    # procedura
    # res = check_id_exists(1, 0, 'Projects', result)
    # print(res)
    # add_project(20, 'Carter Inc', '23-06-2018', '29-03-2020')
    # add_department(11, 'Sales', 'Chicago', 'United States')
    # add_job (1, 'CEO', 15000)
    # add_employee(1, 41, 12, 3, 'Anabel', 'Creddon', '14-03-1987', 3313, '06-03-2010')
    # add_employee_project(160, 305)
