CREATE DATABASE employees;
USE employees;
DROP PROCEDURE IF EXISTS create_tables;

-- create tables
DELIMITER //
CREATE PROCEDURE create_tables()
BEGIN
	CREATE TABLE IF NOT EXISTS Projects (
		ID INT NOT NULL,
		Name VARCHAR(40) NOT NULL UNIQUE,
		StartDate DATE,
		EndDate DATE,
		PRIMARY KEY (ID)
	);

	CREATE TABLE IF NOT EXISTS Departments (
		ID INT NOT NULL,
		Name VARCHAR(40) NOT NULL,
		City VARCHAR(40) NOT NULL,
		Country VARCHAR(40) NOT NULL,
		PRIMARY KEY (ID)
	);

	CREATE TABLE IF NOT EXISTS Jobs (
		ID INT NOT NULL,
		JobTitle VARCHAR(40) NOT NULL UNIQUE,
		Sal FLOAT NOT NULL,
		PRIMARY KEY (ID)
	);

	CREATE TABLE IF NOT EXISTS Employees (
		EmpID INT NOT NULL,
		DepID INT NOT NULL,
		MgrID INT,
		JobID INT NOT NULL,
		FirstName VARCHAR(40) NOT NULL,
		LastName VARCHAR(40) NOT NULL,
		BirthDate DATE NOT NULL,
		Bonus FLOAT NOT NULL,
		Hiredate DATE NOT NULL,
		PRIMARY KEY (EmpID),
		FOREIGN KEY (DepID) REFERENCES Departments(ID) ON DELETE CASCADE ON UPDATE CASCADE,
		FOREIGN KEY (MgrID) REFERENCES Employees(EmpID) ON DELETE CASCADE ON UPDATE CASCADE,
		FOREIGN KEY (JobID) REFERENCES Jobs(ID) ON DELETE CASCADE ON UPDATE CASCADE
	);

	CREATE TABLE IF NOT EXISTS EmployeeProject (
		ProjectID INT NOT NULL,
		EmpID INT NOT NULL,
		FOREIGN KEY (ProjectID) REFERENCES Projects(ID) ON DELETE CASCADE ON UPDATE CASCADE,
		FOREIGN KEY (EmpID) REFERENCES Employees(EmpID) ON DELETE CASCADE ON UPDATE CASCADE
	);
END //
DELIMITER ;

CALL create_tables();
