import requests
import sys

URL = 'noguiserver'
PORT = '5555'

def client_root():
    while (True):
        print(
            "\nWelcome to user menu (read-only rights)! Here are your options (Choose one):\n"
            "1. Show Projects\n2. Show Departments\n3. Show Jobs\n4. Show Employees\n"
            "5. Show Employees Detailed\n6. Select Project\n7. Select Department\n8. Select Job\n9. Select Employee\n"
            "10. Select Employee Detailed\n11. Show Employees With Highest Number Of Projects\n"
            "12. Show Employees With Maximum Salary In A Given Department\n"
            "13. Show Employees with certain seniority\n14. Exit\n\n")
        line = input()
        if line == "1":
            show_projects()
        elif line == "2":
            show_departments()
        elif line == "3":
            show_jobs()
        elif line == "4":
            show_employees()
        elif line == "5":
            show_employees_detailed()
        elif line == "6":
            print("\nInsert parameters for project:\n")
            projId = input()
            select_project(projId)
        elif line == "7":
            print("\nInsert parameters for department:\n")
            depId = input()
            select_department(depId)
        elif line == "8":
            print("\nInsert parameters for job:\n")
            jobId = input()
            select_job(jobId)
        elif line == "9":
            print("\nInsert parameters for employee:\n")
            empId = input()
            select_employee(empId)
        elif line == "10":
            print("\nInsert parameters for employee:\n")
            empId = input()
            select_employee_detailed(empId)
        elif line == "11":
            highest_nr_projects()
        elif line == "12":
            print("\nInsert department ID:\n")
            depId = input()
            emp_max_sal_in_dep(depId)
        elif line == "13":
            print("\nInsert seniority:\n")
            seniority = input()
            get_employees_seniority(seniority)
        else:
            print("Invalid option! Choose a correct option!")


def show_projects():
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-projects")
    # data = response.json()
    # print(data)
    # if data.get('result') == None:
    #     print("\nProjects table is empty!\n")
    #     return None
    # else:
    #     return data

def show_departments():
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-departments")
    data = response.json()
    if data.get('result') == None:
        print("\nDepartments table is empty!\n")
        return None
    else:
        return data

def show_jobs():
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-jobs")
    data = response.json()
    if data.get('result') == None:
        print("\nJobs table is empty!\n")
        return None
    else:
        return data

def show_employees():
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-employees")
    data = response.json()
    if data.get('result') == None:
        print("\nEmployees table is empty!\n")
        return None
    else:
        return data

def show_employees_detailed():
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-employees-info")
    data = response.json()
    if data.get('result') == None:
        print("\nEmployees table is empty!\n")
        return None
    else:
        return data

def select_project(projId):
    payload = {
        'id': projId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/select-project", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no project with given id!\n")
        return None
    else:
        return data

def select_department(depId):
    payload = {
        'id': depId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/select-department", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no department with given id!\n")
        return None
    else:
        return data

def select_job(jobId):
    payload = {
        'id': jobId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/select-job", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no job with given id!\n")
        return None
    else:
        return data

def select_employee(empId):
    payload = {
        'id': empId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/select-employee", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no employee with given id!\n")
        return None
    else:
        return data

def select_employee_detailed(empId):
    payload = {
        'id': empId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/select-employee-detailed", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no employee with given id!\n")
        return None
    else:
        return data

def highest_nr_projects():
    response = requests.get(url="http://" + URL + ":" + PORT + "/most-active-employees")
    data = response.json()
    if data.get('result') == None:
        print("\nEmployees table empty!\n")
        return None
    else:
        return data

def emp_max_sal_in_dep(depId):
    payload = {
        'id': depId
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/most-rewarded-employees", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no department with given id / Department is empty!\n")
        return None
    else:
        return data

def get_employees_seniority(seniority):
    payload = {
        'id': seniority
    }
    response = requests.get(url="http://" + URL + ":" + PORT + "/show-senior-employees", params=payload)
    data = response.json()
    if data.get('result') == None:
        print("\nThere is no employee with given seniority!\n")
        return None
    else:
        return data



if __name__ == '__main__':
    client_root()