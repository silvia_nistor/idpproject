USE employees;
DROP PROCEDURE IF EXISTS select_project;
DROP PROCEDURE IF EXISTS select_department;
DROP PROCEDURE IF EXISTS select_job;
DROP PROCEDURE IF EXISTS select_employee;
DROP PROCEDURE IF EXISTS select_employee_detailed;


DELIMITER //
CREATE PROCEDURE select_project(IN projectId INT)
BEGIN
	SELECT * FROM Projects WHERE Projects.ID = projectId;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE select_department(IN departmentId INT)
BEGIN
	SELECT * FROM Departments WHERE Departments.ID = departmentId;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE select_job(IN jobId INT)
BEGIN
	SELECT * FROM Jobs WHERE Jobs.ID = jobId;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE select_employee(IN employeeId INT)
BEGIN
	SELECT * FROM Employees WHERE Employees.EmpID = employeeId;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE select_employee_detailed(IN employeeId INT)
BEGIN
    SELECT * FROM (
        SELECT
          e.EmpID AS "EmployeeID"
          , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
          , DATE_FORMAT(e.BirthDate, '%d-%m-%Y') AS "Birth Date"
          , DATE_FORMAT(e.Hiredate, '%d-%m-%Y') AS "Hire Date"
          , CONCAT('$', FORMAT(j.Sal, 2)) AS "Salary"
          , CONCAT('$', FORMAT(e.Bonus, 2)) AS "Bonus "
          , j.JobTitle AS "Current Job"
          , GROUP_CONCAT(ep.ProjectID SEPARATOR ', ') AS "Current Projects"
          , CONCAT(m.FirstName, " ", m.LastName) AS "Manager Name"
          , d.Name AS "Department"
          , d.City AS "City"
          , d.Country AS "Country"
        FROM Employees e
        JOIN Jobs j
            ON e.JobID = j.ID
          LEFT JOIN Employees m
            ON e.MgrID = m.EmpID
          INNER JOIN Departments d
            ON d.ID = e.DepID
          LEFT JOIN EmployeeProject ep
            ON e.EmpID = ep.EmpID
        GROUP BY e.EmpID
        ORDER BY e.EmpID) AS tmpEmp
    WHERE tmpEmp.EmployeeID = employeeId;
END //
DELIMITER ;
