
USE employees;
DROP PROCEDURE IF EXISTS check_id_exists;
DROP PROCEDURE IF EXISTS update_bonus;
DROP PROCEDURE IF EXISTS employees_info;
DROP PROCEDURE IF EXISTS highest_nr_projects;
DROP PROCEDURE IF EXISTS nr_projects;
DROP PROCEDURE IF EXISTS emp_max_sal_in_dep;
DROP PROCEDURE IF EXISTS get_employees_with_seniority;
DROP FUNCTION IF EXISTS compute_seniority;
DROP FUNCTION IF EXISTS nr_emp_with_seniority;
DROP TRIGGER IF EXISTS before_delete_project;
DROP TRIGGER IF EXISTS before_delete_department;
DROP TRIGGER IF EXISTS before_delete_job;
DROP TRIGGER IF EXISTS before_delete_employee;



DELIMITER //
CREATE TRIGGER before_delete_project
	AFTER DELETE
	ON Projects
	FOR EACH ROW
BEGIN
	DECLARE value INT;

	SELECT COUNT(*) INTO value FROM EmployeeProject WHERE EmployeeProject.ProjectID = OLD.ID;

	IF value > 0 THEN
		DELETE FROM EmployeeProject
		WHERE EmployeeProject.ProjectID = OLD.ID;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER before_delete_department
	BEFORE DELETE
	ON Departments
	FOR EACH ROW
BEGIN
	DECLARE value INT;

	SELECT COUNT(*) INTO value FROM Employees WHERE Employees.DepID = OLD.ID;

	IF value > 0 THEN
			SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'Cannot delete Department';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER before_delete_job
	BEFORE DELETE
	ON Jobs
	FOR EACH ROW
BEGIN
	DECLARE value INT;

	SELECT COUNT(*) INTO value FROM Employees WHERE Employees.JobID = OLD.ID;

	IF value > 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete Job';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER before_delete_employee
	AFTER DELETE
	ON Employees
	FOR EACH ROW
BEGIN
	DECLARE value INT;

	SELECT COUNT(*) INTO value FROM EmployeeProject WHERE EmployeeProject.EmpID = OLD.EmpID;

	IF value > 0 THEN
		DELETE FROM EmployeeProject
		WHERE EmployeeProject.EmpID = OLD.EmpID;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE check_id_exists(
	IN id1 INT, IN id2 INT, IN tableName VARCHAR(40), OUT result INT
)
BEGIN
	IF tableName = 'Projects' THEN
		SELECT COUNT(*) INTO result FROM Projects WHERE Projects.ID = id1;
	ELSEIF tableName = 'Departments' THEN
		SELECT COUNT(*) INTO result FROM Departments WHERE Departments.ID = id1;
	ELSEIF tableName = 'Jobs' THEN
		SELECT COUNT(*) INTO result FROM Jobs WHERE Jobs.ID = id1;
	ELSEIF tableName = 'Employees' THEN
		SELECT COUNT(*) INTO result FROM Employees WHERE Employees.EmpID = id1;
	ELSEIF tableName = 'EmployeeProject' THEN
		SELECT COUNT(*) INTO result FROM EmployeeProject WHERE EmployeeProject.ProjectID = id1 AND EmployeeProject.EmpID = id2;
	END IF;
END //
DELIMITER ;


-- change bonus according to seniority
DELIMITER //
CREATE PROCEDURE update_bonus()
BEGIN
	DECLARE value INT;
	DECLARE emp_id INT;
	DECLARE job_id INT;
	DECLARE job VARCHAR(40);
	DECLARE finished INT DEFAULT 0;
	DECLARE emp_cursor CURSOR FOR SELECT EmpID, JobID from Employees;

	DECLARE CONTINUE HANDLER
		FOR NOT FOUND SET finished = 1;


	OPEN emp_cursor;
	get_emp: LOOP
		FETCH emp_cursor INTO emp_id, job_id;

		IF finished = 1 THEN
			LEAVE get_emp;
			END IF;

		SELECT JobTitle into job FROM Jobs WHERE ID = job_id;
		SET value = compute_seniority(emp_id) DIV 10;
		--
		CASE job
			WHEN 'CEO' THEN SET value = value * 3;
			WHEN 'Manager' THEN SET value = value * 2;
			ELSE
				BEGIN
				END;
		END CASE;

		UPDATE Employees
		SET Bonus = Bonus + 0.1 * value * Bonus
		WHERE EmpID = emp_id;

	END LOOP get_emp;
	CLOSE emp_cursor;
END //
DELIMITER ;

-- compute seniority for an employee
DELIMITER //
CREATE FUNCTION compute_seniority(empID INT)
RETURNS INT
DETERMINISTIC
BEGIN
	DECLARE empHireDate DATE;
	DECLARE seniority INT;

	SELECT Hiredate INTO empHireDate FROM Employees WHERE Employees.EmpID = empID;
	SET seniority = YEAR(CURDATE()) - YEAR(empHireDate);
	RETURN seniority;
END //
DELIMITER ;

-- find how many employees have a certain seniority
DELIMITER //
CREATE FUNCTION nr_emp_with_seniority(seniority INT)
RETURNS INT
DETERMINISTIC
BEGIN
	DECLARE emp_id INT;
	DECLARE finished INT DEFAULT 0;
	DECLARE value INT;
	DECLARE nr_emp INT DEFAULT 0;
	DECLARE emp_cursor CURSOR FOR SELECT EmpID from Employees;

	DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET finished = 1;

	OPEN emp_cursor;
	get_emp: LOOP
		FETCH emp_cursor INTO emp_id;
		IF finished = 1 THEN
			LEAVE get_emp;
		END IF;
		SET value = compute_seniority(emp_id);
		IF value = seniority THEN
			SET nr_emp = nr_emp + 1;
		END IF;
	END LOOP get_emp;
	CLOSE emp_cursor;
	RETURN nr_emp;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_employees_with_seniority(IN seniority INT)
BEGIN
	SELECT
	  e.EmpID AS "Employee #"
	  , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
	  , DATE_FORMAT(e.BirthDate, '%d-%m-%Y') AS "Birth Date"
	  , DATE_FORMAT(e.Hiredate, '%d-%m-%Y') AS "Hire Date"
	  , CONCAT('$', FORMAT(j.Sal, 2)) AS "Salary"
	  , CONCAT('$', FORMAT(e.Bonus, 2)) AS "Bonus "
	  , j.JobTitle AS "Current Job"
	  , GROUP_CONCAT(ep.ProjectID SEPARATOR ', ') AS "Current Projects"
	  , CONCAT(m.FirstName, " ", m.LastName) AS "Manager Name"
	  , d.Name AS "Department"
	  , d.City AS "City"
	  , d.Country AS "Country"
	FROM Employees e
	JOIN Jobs j
	    ON e.JobID = j.ID
	  LEFT JOIN Employees m
	    ON e.MgrID = m.EmpID
	  INNER JOIN Departments d
	    ON d.ID = e.DepID
	  LEFT JOIN EmployeeProject ep
	    ON e.EmpID = ep.EmpID
	GROUP BY e.EmpID
	HAVING compute_seniority(e.EmpID) = seniority
	ORDER BY e.EmpID;
END //
DELIMITER ;


-- detailed infos about each employee
DELIMITER //
CREATE PROCEDURE employees_info()
BEGIN
	SELECT
	  e.EmpID AS "Employee #"
	  , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
	  , DATE_FORMAT(e.BirthDate, '%d-%m-%Y') AS "Birth Date"
	  , DATE_FORMAT(e.Hiredate, '%d-%m-%Y') AS "Hire Date"
	  , CONCAT('$', FORMAT(j.Sal, 2)) AS "Salary"
	  , CONCAT('$', FORMAT(e.Bonus, 2)) AS "Bonus "
	  , j.JobTitle AS "Current Job"
	  , GROUP_CONCAT(ep.ProjectID SEPARATOR ', ') AS "Current Projects"
	  , CONCAT(m.FirstName, " ", m.LastName) AS "Manager Name"
	  , d.Name AS "Department"
	  , d.City AS "City"
	  , d.Country AS "Country"
	FROM Employees e
	JOIN Jobs j
	    ON e.JobID = j.ID
	  LEFT JOIN Employees m
	    ON e.MgrID = m.EmpID
	  INNER JOIN Departments d
	    ON d.ID = e.DepID
	  LEFT JOIN EmployeeProject ep
	    ON e.EmpID = ep.EmpID
	GROUP BY e.EmpID
	ORDER BY e.EmpID;
END //
DELIMITER ;

-- get number of projects for every employee
DELIMITER //
CREATE PROCEDURE nr_projects()
BEGIN
	SELECT
	  e.EmpID AS "Employee #"
	  , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
	  , DATE_FORMAT(e.BirthDate, '%d-%m-%Y') AS "Birth Date"
	  , DATE_FORMAT(e.Hiredate, '%d-%m-%Y') AS "Hire Date"
	  , j.JobTitle AS "Current Job"
	  , COUNT(ep.ProjectID) AS "Projects nr."
	  , CONCAT(m.FirstName, " ", m.LastName) AS "Manager Name"
	  , d.Name AS "Department"
	FROM Employees e
	JOIN Jobs j
		ON e.JobID = j.ID
	  LEFT JOIN Employees m
		ON e.MgrID = m.EmpID
	  INNER JOIN Departments d
		ON d.ID = e.DepID
	  LEFT JOIN EmployeeProject ep
		ON e.EmpID = ep.EmpID
	GROUP BY e.EmpID
	ORDER BY COUNT(ep.ProjectID) DESC;
END //
DELIMITER ;


-- get the employees with the highest number of projects
DELIMITER //
CREATE PROCEDURE highest_nr_projects()
BEGIN
	SELECT
	  e.EmpID AS "Employee #"
	  , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
	  , DATE_FORMAT(e.BirthDate, '%d-%m-%Y') AS "Birth Date"
	  , DATE_FORMAT(e.Hiredate, '%d-%m-%Y') AS "Hire Date"
	  , CONCAT('$', FORMAT(j.Sal, 2)) AS "Salary"
	  , CONCAT('$', FORMAT(e.Bonus, 2)) AS "Bonus "
	  , j.JobTitle AS "Current Job"
	  , COUNT(ep.ProjectID) AS "Projects nr."
	  , CONCAT(m.FirstName, " ", m.LastName) AS "Manager Name"
	  , d.Name AS "Department"
	  , d.City AS "City"
	  , d.Country AS "Country"
	FROM Employees e
	JOIN Jobs j
	    ON e.JobID = j.ID
	  LEFT JOIN Employees m
	    ON e.MgrID = m.EmpID
	  INNER JOIN Departments d
	    ON d.ID = e.DepID
	  LEFT JOIN EmployeeProject ep
	    ON e.EmpID = ep.EmpID
	GROUP BY e.EmpID
	HAVING COUNT(ep.ProjectID) = (
	    SELECT
	        COUNT(ep.ProjectID)
	    FROM Employees e
	    JOIN Jobs j
	        ON e.JobID = j.ID
	      LEFT JOIN Employees m
	        ON e.MgrID = m.EmpID
	      INNER JOIN Departments d
	        ON d.ID = e.DepID
	      LEFT JOIN EmployeeProject ep
	        ON e.EmpID = ep.EmpID
	    GROUP BY e.EmpID
	    ORDER BY COUNT(ep.ProjectID) DESC LIMIT 1);
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE emp_max_sal_in_dep(IN dep_id INT)
BEGIN
	SELECT
	  e.EmpID AS "Employee #"
	  , CONCAT(e.FirstName, " ", e.LastName) AS "Name"
	  , j.Sal
	FROM Employees e
	JOIN Jobs j
	  ON e.JobID = j.ID
	HAVING j.Sal =
	(SELECT Salary FROM (
	SELECT
	    e.DepID,
	    MAX(j.Sal) AS "Salary"
	FROM Employees e
	JOIN Jobs j
	    ON e.JobID = j.ID
	GROUP BY e.DepID
	ORDER BY e.DepID) AS S WHERE S.DepID = dep_id);
END //
DELIMITER ;
